all: svr svr-mp svr-pool svc clt clt-deny qrac-clt qrac-svr

svr: hello-svr.c hello.h
	gcc -o svr hello-svr.c

svr-mp: hello-svr-mp.c hello.h
	gcc -o svr-mp hello-svr-mp.c

svr-pool: hello-svr-pool.c hello.h
	gcc -o svr-pool hello-svr-pool.c

svc: hello-svc.c hello-to.h
	gcc -g -Wall -o svc hello-svc.c

clt: hello-clt.c hello.h
	gcc -o clt hello-clt.c

clt-deny: hello-clt-deny.c hello.h
	gcc -o clt-deny hello-clt-deny.c

qrac-svr: qrac-svr.c qrac.h
	gcc -o qrac-svr-fs qrac-svr.c

qrac-clt: qrac-clt.c qrac.h
	gcc -o qrac-clt-fs qrac-clt.c

run.server:
	./svr 2000

run.client:
	./clt 127.0.0.1 2000

clean:
	rm svr
	rm svr-mp
	rm svr-pool
	rm svc
	rm clt
	rm clt-deny
	rm qrac-svr
	rm qrac-clt