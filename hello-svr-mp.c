/*=========================================================================
 * $cours  : TP réseau
 * $section: protocole hello
 *      $Id: hello-svr.c 451 2018-03-04 14:11:48Z ia $ 
 * $HeadURL: svn://lunix120.ensiie.fr/ia/cours/reseau/programmation/src/hello-svr.c $
 *  $Author: Ivan Auge (Email: auge@ensiie.fr)
**=======================================================================*/

/*=======================================================================*/
/*= Implémentation d'un serveur "hello" en TCP/IPv4                     =*/
/*= Le protocole "hello" est décrit dans le header hello.h              =*/
/*=                                                                     =*/
/*= Usage:                                                              =*/
/*=    $ ./a.out port                                                   =*/
/*=                                                                     =*/
/*= Paramètre:                                                          =*/
/*=    port: le port du serveur qui est un entier positif.              =*/
/*=======================================================================*/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "hello.h"

/**
 * La fonction gest_sigpipe permet d'indiquer au système 
 * si le signal SIGPIPE arrive
 */
void gest_sigpipe(int sig)
{
  fprintf(stderr, " client déconnecté\n");
}

int main(int argc, char** argv)
{
  prgname = argv[0];
  int statut;
  pid_t pid;

  /* vérification des arguments */
  if ( argc < 2 ) {
      fprintf(stderr,"%s:usage: %s port\n",prgname,prgname);
      exit(1);
  }
  char* service = argv[1];
  int duree = argc == 2 ? 0 : atoi(argv[2]) * 100000;

  // Vérifie que la durée n'est pas négative
  if (duree < 0) duree = 0;

  signal(SIGPIPE, gest_sigpipe);

  /* création du SAP des clients */
  socklen_t       cltsSAPlen;
  struct sockaddr cltsSAP;
  getTCPsap(&cltsSAP, &cltsSAPlen, NULL, service);

  /* création de l'automate de connexion */
  int sock;
  if ( (sock = socket(AF_INET, SOCK_STREAM, 0)) == -1 ) {
    fprintf(stderr, "%s: pb socket: %s\n", argv[0], strerror(errno));
    exit(1);
  }
  if ( bind(sock, &cltsSAP, cltsSAPlen) < 0 ) {
    fprintf(stderr, "%s: pb bind: %s\n", argv[0], strerror(errno));
    exit(1);
  }
  if ( listen(sock, 100) != 0 ) {
     fprintf(stderr, "%s: pb listen:%s\n", argv[0], strerror(errno));
     exit(1);
  }

  // Il faut ignorer le signal du fils quand il a termminé son job pour terminer le process
  signal(SIGCHLD, SIG_IGN);

  // Boucle infinie du serveur
  while (1) {
    int cx;
    struct sockaddr cltSAP;
    socklen_t       cltSAPlen = sizeof(cltSAPlen);
    char PDU[100];

    /* creation du flux de communication (cx) */
    if ( (cx = accept(sock, &cltSAP, &cltSAPlen)) == -1 ) {
      fprintf(stderr,"%s: pb accept : sock=%d : %s\n", argv[0], sock, strerror(errno));
      exit(1);
    }

    // Création du processus enfant
    pid = fork();

    if (pid == -1) {
      fprintf(stderr, "Erreur dans le fork\n");
      exit(EXIT_FAILURE);
    } 
    // Dans le fils
    else if (pid == 0) {
      /* Dialogue état début */
      sprintf(PDU, "HELLO");
      statut = write(cx, PDU, 6);
      if ( statut == -1 ) { 
        close(cx); exit(1); 
      }
      printf("serveur: envoyé \"HELLO\" : "); fflush(stdout);

      // Exercice 2 - Q1
      //close(cx); continue;

      // Exercice 3 - Q1: Simulation des temps de traitement
      if(duree != 0) usleep(duree);
      
      /* Dialogue état att-ok */
      statut = lire_PDU(PDU, cx);
      if ( statut != 'O' ) goto error;
      
      printf("reçu \"%s\" :", PDU); fflush(stdout);
      
      sprintf(PDU, "FIN");
      statut = write(cx, PDU, 4);
      if ( statut==-1 ) { close(cx); exit(1); }
      printf(" envoyé \"FIN\" : Quitte\n");

      /* Terminaison du dialogue (état fin) */
      close(cx);
      fprintf(stdout, "Fermeture cx \n");
      exit(0); // Il faut faire un exit pour terminer le process

      fprintf(stdout, "Fin \n");

      error:
        close(cx);
        fprintf(stderr,"%s: message de type %c est inattendu\n",
                prgname,statut);
        exit(1); // Il faut faire un exit pour terminer le process
    }
    // Dans le père
    else {
      close(cx);
      
    }
  }

  close(sock);
  return 0;
}
/*=======================================================================*/
